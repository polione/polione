<?php
namespace  Tests\Stubs;

use App\SearchProviders\SearchProvider;
use App\Collections\DownloadableCollection;
use PHPUnit\Util\Exception;

class ExceptionConcreteFakeSearchProvider extends SearchProvider
{
    public function search(string $query = null): DownloadableCollection
    {
       throw new Exception('Fake Exception');
    }
}
