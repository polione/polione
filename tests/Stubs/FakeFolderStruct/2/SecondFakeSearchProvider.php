<?php

namespace Tests\Stubs;

use App\Collections\DownloadableCollection;
use App\SearchProviders\SearchProvider;
use Tests\Stubs\FakeDownloadable;

class SecondFakeSearchProvider extends SearchProvider
{

    public function search(string $query = null): DownloadableCollection
    {
        $results = new DownloadableCollection();

        if ($query === 'casa') {
            $results[] = new FakeDownloadable();
        }

        return $results;
    }
}
