<?php


namespace Tests\Stubs;


use App\SearchProviders\Downloadable;
use Illuminate\Support\Facades\Log;

class FakeDownloadable extends Downloadable
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Process download task
        Log::info('downloading..');
        sleep(1);
        Log::info('done..');
    }

    public function getFileName(): string
    {
        return 'Fake File Name';
    }

    public function getDirectory(): string
    {
       return $this->directory;
    }

    public function getTitle(): string
    {
        return 'Fake Downloadble';
    }
}
