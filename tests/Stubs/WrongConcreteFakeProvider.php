<?php
namespace  Tests\Stubs;

use App\SearchProviders\SearchProvider;
use App\Collections\DownloadableCollection;

class WrongConcreteFakeProvider extends SearchProvider
{

    public function search(string $query = null): DownloadableCollection
    {
        $results = new DownloadableCollection();

       if ($query === 'casa'){
           $results[] = 'Wrong';
       }

       return $results;
    }
}
