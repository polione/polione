<?php

namespace Tests\Unit;

use App\SearchProviders\Horrible\HorribleDownloadable;
use App\SearchProviders\Horrible\HorribleSearchProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class HorribleProviderTest extends TestCase
{
    use HandleTestTrait;
    use DatabaseMigrations;

    public function getDownloadable()
    {
        return HorribleDownloadable::class;
    }

    public function getSearchExpectedCount()
    {
        return [ "Houkago Teibou Nisshi" => 2, "houkago teibou nisshi" => 2] ;
    }

    public function getSearchProvider()
    {
        return new HorribleSearchProvider(
            __DIR__ . '/../Stubs/fakeHorribleSubsFeed.xml'
        );
    }
}