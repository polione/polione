<?php

namespace Tests\Unit;

use App\DirectoryManager\DirectoryCategory;
use App\Facades\DirectoryManager;
use App\SearchProviders\ShowRssInfo\ShowRssDownloadable;
use Tests\Stubs\FakeDownloadable;
use Tests\TestCase;

class DirectoryManagerTest extends TestCase
{

    public function setUp() : void
    {
        parent::setUp();

        putenv(DirectoryCategory::MOVIE()->getEnvKey() . '=disk4');
        putenv(DirectoryCategory::ANIME()->getEnvKey() . '=disk7');
        putenv(DirectoryCategory::TVSHOW()->getEnvKey() . '=disk5');
        putenv(DirectoryCategory::MISC()->getEnvKey() . '=disk3');
    }

    /** @test */
    public function it_should_return_path_with_a_downloadable()
    {
        $fakeDownloadable =  new FakeDownloadable();

        $directory = DirectoryManager::with($fakeDownloadable);

        self::assertEquals('/media/disk3/Misc', $directory);
    }

    /** @test */
    public function it_should_handle_movie_category()
    {
        $fakeDownloadable =  new FakeDownloadable();

        $fakeDownloadable->setCategory('Movies');

        $directory = DirectoryManager::with($fakeDownloadable);

        self::assertEquals('/media/disk4/Movies', $directory);
    }

    /** @test */
    public function it_should_handle_movie_tvshow()
    {
        $downloadable = new ShowRssDownloadable('La Casa di Carta', '');
        $downloadable->season = 1;
        $downloadable->name = 'La Casa di Carta';

        $directory = DirectoryManager::with($downloadable);
        self::assertEquals('/media/disk5/TV Shows/La Casa di Carta/Season 1', $directory);
    }

    /** @test */
    public function it_should_handle_anime()
    {
        $fakeDownloadable =  new FakeDownloadable();
        $fakeDownloadable->setCategory('Anime');
        $fakeDownloadable->season = 1;
        $fakeDownloadable->name = 'La Casa di Carta';

        $directory = DirectoryManager::with($fakeDownloadable);
        self::assertEquals('/media/disk7/Anime/La Casa di Carta/Season 1', $directory);
    }
}
