<?php

namespace Tests\Unit;
use App\SearchProviders\HdTorrents\HdTorrentsDownloadble;
use App\SearchProviders\HdTorrents\HdTorrentsSearchProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class HdTorrentsProviderTest extends TestCase
{
    use DatabaseMigrations, HandleTestTrait;

    public function getDownloadable()
    {
        return HdTorrentsDownloadble::class;
    }

    public function getSearchExpectedCount()
    {
        return ['criminale' => 1, 'Criminale' => 1];
    }

    public function getSearchProvider()
    {
        return (new HdTorrentsSearchProvider(__DIR__. '/../Stubs/fakeHdTorrentsRss.xml'));
    }
}