<?php


namespace Tests\Unit\Commands;


use App\Models\Provider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Tests\Stubs\SearchProvider;

class ProvidersCommandTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */

    public function it_should_list_providers()
    {
        factory(Provider::class)->create([
            'type' => SearchProvider::class,
            'link' => __DIR__ . '../../Stubs/fakeShowRssFeed.xml'
        ]);

        $this->artisan("providers")->assertExitCode(0);
    }
}
