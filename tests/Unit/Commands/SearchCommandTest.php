<?php


namespace Tests\Unit\Commands;

use App\Models\Provider;
use App\Polione;
use App\SearchProviders\ShowRssInfo\ShowRssInfoSearchProvider;
use App\Support\Range;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Tests\Stubs\ExceptionConcreteFakeSearchProvider;

class SearchCommandTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_should_search_an_empty_string()
    {
        $this->artisan('search')->assertExitCode(0);
    }

    /**
     * @test
     */
    public function it_should_search_a_query()
    {
        $this->artisan('search "Will & Grace"')

            ->assertExitCode
            (0);
    }

    /**
     * @test
     */
    public function it_should_search_a_query_and_return()
    {
        $query = "Will & Grace";

        $this->app->bind(Polione::class, function () use ($query) {
            return \Mockery::mock(new Polione())->shouldReceive('search')->with($query)->andReturn()->getMock();
        });

        $this->artisan("search '$query'")->assertExitCode(0);
    }

    /**
     * @test
     */
    public function it_should_read_providers_from_database()
    {
        factory(Provider::class)->create([
            'type' => ShowRssInfoSearchProvider::class,
            'link' => __DIR__ . '../../../Stubs/fakeShowRssFeed.xml'
        ]);

        $this->assertDatabaseHas('providers', [
            'type' => ShowRssInfoSearchProvider::class,
            'link' => __DIR__ . '../../../Stubs/fakeShowRssFeed.xml'
        ]);

        $query = 'Will and Grace S11E13 1080p';

        $this->artisan("search '$query'")
            ->expectsQuestion('What is your choice?', 1)
            ->expectsOutput('Will and Grace S11E13 dispatched!');
    }

    /**
     * @test
     */
    public function it_should_fail_gracefully_with_wrong_type()
    {
        factory(Provider::class)->create([
            'type' => 'FakeClass',
            'link' => __DIR__ . '../../../Stubs/fakeShowRssFeed.xml'
        ]);

        $this->artisan("search")->assertExitCode(0);
    }

    /**
     * @test
     */
    public function it_should_fail_gracefully_with_thrown_exception_in_providers()
    {
        factory(Provider::class)->create([
            'type' => ExceptionConcreteFakeSearchProvider::class,
            'link' => __DIR__ . '../../../Stubs/fakeShowRssFeed.xml'
        ]);

        $this->artisan("search casa")->assertExitCode(0);
    }

    /**
     * @test
     */
    public function it_should_pick_correct_range()
    {
        $range = Range::get('1-3', [1, 2, 3, 4]);
        $this->assertEquals([1, 2, 3], $range);

        $range = Range::get('2,4', [5, 6, 7, 8]);
        $this->assertEquals([6, 7, 8], $range);

        $range = Range::get('2,3', [9, 10, 11, 12]);
        $this->assertEquals([10, 11], $range);

        $range = Range::get('okn3f', [9, 10, 11, 12]);
        $this->assertEquals([], $range);

        $range = Range::get('1', [9, 10, 11, 12]);
        $this->assertEquals([9], $range);
    }

}
