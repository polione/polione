<?php


namespace Tests\Unit\Commands;


use App\Models\Provider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Tests\Stubs\SearchProvider;

class DiscoveryCommandTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */

    public function it_should_discover_providers()
    {
        Provider::create([
            'type' => "\\App\SearchProviders\HdTorrents\HdTorrentsSearchProvider",
            'link' => "",
            'enabled' => true
        ]);


        $this->assertEquals('\\App\SearchProviders\HdTorrents\HdTorrentsSearchProvider',Provider::all()->first()->type);
        $this->assertEquals('',Provider::all()->first()->link);
        $this->artisan("discovery")->expectsQuestion('Configure all providers?', 'no')->assertExitCode(0);
        
    }
}
