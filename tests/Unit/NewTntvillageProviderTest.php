<?php

namespace Tests\Unit;

use App\SearchProviders\NewTntVillage\NewTntVillageDownloadable;
use App\SearchProviders\NewTntVillage\NewTntVillageSearchProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class NewTntvillageProviderTest extends TestCase
{
    use DatabaseMigrations;
    use HandleTestTrait;

    public function getSearchProvider()
    {
        return new NewTntVillageSearchProvider(
            __DIR__ . '/../Stubs/fakeTntVillage.html'
        );
    }

    public function getDownloadable()
    {
       return NewTntVillageDownloadable::class;
    }

    public function getSearchExpectedCount()
    {
        return [
            '' => 21,
        ];

    }

    /**
     * @test
     */
    public function it_should_bypass_origin()
    {
        $provider = new NewTntVillageSearchProvider();
        $result = $provider->search('scrubs');
        self::assertGreaterThanOrEqual(4, count($result));
    }

    /**
     * @test
     */
    public function it_should_fix_encoding()
    {
        $provider = new NewTntVillageSearchProvider();
        $result = $provider->search('la casa di bamb');
        self::assertGreaterThanOrEqual(1, count($result));
        self::assertStringContainsString('bambù', $result[0]->getTitle());
    }

}