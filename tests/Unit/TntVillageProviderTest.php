<?php

namespace Tests\Unit;

use App\Contracts\SearchProviders\Downloadable;
use App\Facades\PolioneSearch;
use App\Jobs\ProcessDownload;
use App\SearchProviders\TntVillage\TntVillageDownloadable;
use App\SearchProviders\TntVillage\TntVillageSearchProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class TntVillageProviderTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function noResultSearchTest()
    {
        $items = PolioneSearch::addProvider(
            (new TntVillageSearchProvider()))->search('Will and Grace');
        $this->assertIsArray($items);
        $this->assertCount(0, $items);
    }

    /** @test */
    public function exactResultTest()
    {
        $items = PolioneSearch::addProvider(
            (new TntVillageSearchProvider())
        )->search('Will Grace');

        $this->assertIsArray($items);
        $this->assertCount(65, $items);

        $items = PolioneSearch::search('will grace');

        $this->assertIsArray($items);
        $this->assertCount(65, $items);
    }

    /** @test */
    public function arrayOfDownloadableTest()
    {
        $items = PolioneSearch::addProvider(
            (new TntVillageSearchProvider())
        )->search('Will Grace');
        $this->assertInstanceOf(Downloadable::class, $items[0]);
        $this->assertEquals(
            "Will & Grace S10e01-18",
            $items[0]->getTitle()
        );
        $this->assertEquals("Will & Grace", $items[0]->name);
        $this->assertEquals("10", $items[0]->season);
        $this->assertEquals("01-18", $items[0]->episode);
        $this->assertEquals(
            file_get_contents(__DIR__ . "/../Stubs/magnetExpected"),
            $items[0]->getMagnet()
        );
    }

    /** @test */
    public function tntVillageDispatchTest()
    {
        Bus::fake();
        $items = PolioneSearch::addProvider(
            new TntVillageSearchProvider()
        )->search('Will Grace');
        collect($items)->first()->download();

        Bus::assertDispatched(ProcessDownload::class);

        $this->assertIsArray($items);
        $this->assertCount(65, $items);
    }

    /** @test */
    public function tnt_should_call_handle_on_downloadable()
    {
        $mock = \Mockery::mock(TntVillageDownloadable::class)
            ->shouldReceive('getDirectory','getFileName','handle')
            ->andSet('season', 1)
            ->andSet('episode', 1)
            ->getMock();
        ProcessDownload::dispatchNow($mock);
    }

    /** @test */
    public function it_should_handle()
    {

        $tnt = new TntVillageSearchProvider();
        $tnt->search('will grace')[0]->handle();


    }

    /** @test */
    public function it_should_return_attribute()
    {

        $tnt = new TntVillageSearchProvider();
        $downloadble = $tnt->search('will grace')[0];
        $size = $downloadble->size;
        $downloadble->internalCategory = 3;
        self::assertStringContainsStringIgnoringCase(
            'disk3/ebook', $downloadble->path
        );
        $downloadble->internalCategory = 4;
        self::assertStringContainsStringIgnoringCase(
            'disk4/movies', $downloadble->path
        );
        $downloadble->setDescrizione('descrizione');
        self::assertEquals('descrizione', $downloadble->detail);
        self::assertEquals('3.07GB', $size);


    }
}
