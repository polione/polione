<?php

namespace Tests\Unit;

use App\Facades\PolioneSearch;
use App\Jobs\ProcessDownload;
use App\SearchProviders\ShowRssInfo\ShowRssInfoSearchProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Bus;
use Mockery;
use Tests\Stubs\ConcreteFakeSearchProvider;
use Tests\Stubs\FakeDownloadable;
use Tests\TestCase;
use function PHPUnit\Framework\assertEquals;

class DownloadableTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function download_method_should_dispatch_job()
    {
        Bus::fake();
        $items = PolioneSearch::addProvider(new ConcreteFakeSearchProvider())->search('casa');
        collect($items)->first()->download();

        Bus::assertDispatched(ProcessDownload::class);

        $this->assertIsArray($items);
        $this->assertCount(1, $items);
    }

    /**
     * @test
     */
    public function showRssTest()
    {
        Bus::fake();

        $items = PolioneSearch::addProvider(
            new ShowRssInfoSearchProvider(__DIR__ . "/../Stubs/fakeShowRssFeed.xml")
        )->search('Will and Grace');
        collect($items)->first()->download();

        Bus::assertDispatched(ProcessDownload::class);

        $this->assertIsArray($items);
        $this->assertCount(6, $items);
    }

    /**
     * @test
     */
    public function should_call_handle_on_downloadable()
    {
        $mock = Mockery::mock(FakeDownloadable::class)
            ->shouldReceive('getFileName','getDirectory','handle')

            ->getMock();
        ProcessDownload::dispatchNow($mock);
    }

    /**
     * @test
     */
    public function it_should_know_who_is_his_creator()
    {
        $provider = new ConcreteFakeSearchProvider();
        $downloadable = $provider->search('casa');
        $creator = ($downloadable->toArray())[0]->createdBy();
        self::assertEquals('N/A', $creator);
    }

    /**
     * @test
     */
    public function it_should_set_and_get()
    {
        $provider = new FakeDownloadable();
        $provider->setDirectory('dir');
        $provider->setField('field');

        assertEquals('dir', $provider->getDirectory());
        assertEquals('dir', $provider->path);
        assertEquals('dir', $provider->getPathAttribute());
        assertEquals('field', $provider->getField('field'));

        $this->expectException(\RuntimeException::class);
        $provider->nonExistent();


    }
}
