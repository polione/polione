<?php

namespace Tests\Unit;

use App\Facades\PolioneSearch;
use App\Jobs\ProcessDownload;

trait HandleTestTrait
{
    abstract public function getDownloadable();

    abstract public function getSearchExpectedCount();

    abstract public function getSearchProvider();

    /**
     * @test
     */
    public function should_call_handle_on_downloadable()
    {
        $mock = \Mockery::spy(
            $this->getDownloadable()
        );
        ProcessDownload::dispatchNow($mock);
        $mock->shouldHaveReceived('handle');
    }

    /**
     * @test
     */
    public function should_search_and_return_something()
    {
        $expectations = collect($this->getSearchExpectedCount());

        $provider = $this->getSearchProvider();
        $searcher = PolioneSearch::addProvider($provider);
        $expectations->each(
            function ($expectedCount, $query) use ($searcher) {

                $items = $searcher->search(
                    $query
                );
                $this->assertIsArray($items);
                $this->assertCount($expectedCount, $items);
            }
        );
    }
}