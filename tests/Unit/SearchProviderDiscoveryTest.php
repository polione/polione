<?php

namespace Tests\Unit;

use App\Models\Provider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class SearchProviderDiscoveryTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_should_list_available_providers()
    {
        $providers = config('app.search') ?? [];
        self::assertGreaterThan(1, count($providers));


    }

    /** @test */
    public function it_should_create_a_provider()
    {
        Provider::create([
            'type' => "ClassName",
            'link' => "InputString"
        ]);
        $this->assertEquals('ClassName',Provider::all()->first()->type);
        $this->assertEquals('InputString',Provider::all()->first()->link);

    }
    /** @test */
    public function it_enable_and_disable_a_provider()
    {
        Provider::create([
            'type' => "ClassName",
            'link' => "InputString"
        ]);
        Provider::all()->first()->enable();
        $this->assertEquals(1 ,Provider::all()->first()->enabled);
        Provider::all()->first()->disable();
        $this->assertEquals(0,Provider::all()->first()->enabled);

    }
}