<?php

namespace Tests\Unit;

use App\Collections\DownloadableCollection;
use App\Facades\PolioneSearch;
use Tests\Stubs\ConcreteFakeSearchProvider;
use Tests\TestCase;
use Tests\Stubs\SearchProvider;
use Tests\Stubs\FakeDownloadable;
use Tests\Stubs\WrongConcreteFakeProvider;


class SearchProviderTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $items = PolioneSearch::search();

        $this->assertIsArray($items);
    }

    /**
     * @test
     */
    public  function  it_should_call_search_on_providers()
    {
        $provider = \Mockery::mock(new ConcreteFakeSearchProvider());
        $provider->shouldReceive('search')->with('casa')->once()->andReturn(new DownloadableCollection());

        $items = PolioneSearch::addProvider($provider)->search('casa');

        $this->assertIsArray($items);
    }

    /**
     * @test
     */
    public  function  it_should_return_downlodable_array()
    {
        $provider = \Mockery::mock(ConcreteFakeSearchProvider::class);
        $provider->shouldReceive('search')->with('casa')->once()->andReturn(
            new DownloadableCollection(new FakeDownloadable())
        );

        $items = PolioneSearch::addProvider($provider)->search('casa');

        $this->assertIsArray($items);
        $this->assertCount(1,$items);

    }


    /**
     * @test
     */
    public function it_should_fail_with_non_downloadable(){
        $this->expectException(\TypeError ::class);

        $provider = new WrongConcreteFakeProvider();

        $items = PolioneSearch::addProvider($provider)->search('casa');

        $this->assertIsArray($items);
        $this->assertCount(1,$items);
    }

    /**
     * @test
     */
    public  function  it_should_return_downlodable_array_for_real()
    {
        $provider = new ConcreteFakeSearchProvider();

        $items = PolioneSearch::addProvider($provider)->search('casa');

        $this->assertIsArray($items);
        $this->assertCount(1,$items);
    }
}
