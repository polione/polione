<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Collection;
use Mockery\Mock;
use Transmission\Client;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $app->singleton('transmission', function () {
            return \Mockery::mock(Client::class)
                ->shouldReceive('addUrl')
                ->getMock();
        });

        return $app;
    }
}
