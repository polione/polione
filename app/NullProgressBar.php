<?php

namespace App;
class NullProgressBar
{

    /**
     * NullProgressBar constructor.
     */
    public function __construct()
    {
    }

    public function setFormat(string $string)
    {
    }

    public function start()
    {
    }

    public function advance()
    {
    }

    public function finish()
    {
    }
}