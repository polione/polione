<?php

namespace App\Support;

class Loop
{
    public function __construct($query)
    {
        $this->query = $query;
    }

    public static function with($query)
    {
        return new static($query);
    }

    public function do($closure)
    {
        while($this->query != 'exit')
        {
            $this->query = $closure($this->query);
        }
    }
}
