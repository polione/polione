<?php

namespace App\Support;

use Symfony\Component\Console\Helper\Table as SymfonyTable;

class Table
{


    /**
     * @var SymfonyTable
     */
    private SymfonyTable $table;
    /**
     * @var array
     */
    private $headers;

    public function __construct($output, $options)
    {
        $this->table = new SymfonyTable($output);
        $this->table->setStyle('borderless');
        $this->fields = explode(',', $options['with']);
        $this->headers = array_merge(
            ['#', 'Title', 'Source Provider'],
            $this->fields
        );

    }

    public function render(array $results)
    {

        $this->table->setHeaders($this->headers);
        $this->table->setRows($this->convert($results));
        $this->table->render();
    }


    public function convert($results)
    {
        $return = [];
        foreach ($results as $key => $result) {
            $return[] = $this->convertRow($key, $result);
        }
        return $return;
    }

    private function convertRow(int $key, $result)
    {
        $tmp = [];
        $tmp[] = CC::make($key + 1)->yellow();
        $tmp[] = CC::make(trim($result->getTitle()))->bold();
        $tmp[] = CC::make(trim($result->createdBy()))->yellow();
        $tmp = array_merge(
            $tmp,
            collect($this->fields)->map(
                function ($field) use ($result) {
                    return $result->getField($field);
                }
            )->toArray()
        );
        return $tmp;
    }


}