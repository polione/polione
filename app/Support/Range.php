<?php


namespace App\Support;


use Illuminate\Support\Str;

class Range
{
    public static function get(string $range, array $array): array
    {
        $start = 0;
        $end = 0;

        if (Str::contains($range, '-')) {
            [$start, $end] = explode('-', $range);
        } else if (Str::contains($range, ',')) {
            [$start, $end] = explode(',', $range);
        } else if (is_numeric($range) && array_key_exists($range - 1, $array)) {
           return [$array[$range - 1]];
        }

        if ($start > 0 && $end <= count($array)) {
            return array_slice($array, $start - 1, $end - $start + 1);
        }

        return [];
    }
}
