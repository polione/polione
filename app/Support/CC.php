<?php

namespace App\Support;

class CC
{
    /**
     * @var string
     */
    private string $string;

    public function __construct(string $string)
    {
        $this->string = $string;
    }

    public static function make(string $string){
        return new static ($string);
    }

    public static function color($str, $color)
    {
        return "<fg=$color>" . ($str) . "</>";
    }
    public function bold(){
        return "<options=bold>" .  $this->string . "</>";
    }

    public function magenta()
    {
        return CC::color($this->string, 'magenta');
    }

    public function yellow()
    {
        return CC::color($this->string, 'yellow');
    }

    public function green()
    {
        return CC::color($this->string, 'green');
    }
}
