<?php

namespace App\Providers;

use App\Contracts\SearchProviders\SearchProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;

class SearchServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $paths = glob(app_path('SearchProviders') . DIRECTORY_SEPARATOR . "*");
        $paths = array_filter($paths, function($path){

            return is_dir($path);
        });

        if(empty($paths)){
            return;
        }

        $namespace = $this->app->getNamespace();


        foreach ((new Finder)->in($paths)->files() as $provider) {
            $provider = $namespace.str_replace(
                ['/', '.php'],
                ['\\', ''],
                Str::after($provider->getPathName(), app_path().DIRECTORY_SEPARATOR)
            );

            if (is_subclass_of($provider, SearchProvider::class)) {

                $search = config('app.search') ?? [];

                $search[] = $provider;

                config(['app.search' => $search]);
            }
        }
    }
}
