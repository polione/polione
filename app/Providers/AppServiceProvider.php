<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Transmission\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('transmission', function () {
            $client = new Client(
                config('transmission.host'),
                config('transmission.port'),
                config('transmission.username'),
                config('transmission.password')
            );

            if (config('transmission.enableTLS')) {
                $client = $client->enableTLS();
            }

            return $client;
        });

        $this->app->alias('transmission', Client::class);
    }
}
