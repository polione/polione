<?php


namespace App\Collections;

use App\SearchProviders\Downloadable;
use ArrayAccess;
use ArrayIterator;
use Illuminate\Contracts\Support\Arrayable;
use IteratorAggregate;
use Throwable;
use TypeError;

class DownloadableCollection implements ArrayAccess, Arrayable, IteratorAggregate, \Countable
{
    /**
     * @var Downloadable[]
     */
    private $items = [];

    /**
     * DownloadableCollection constructor.
     * @param Downloadable ...$downloadables
     */
    public function __construct(Downloadable ...$downloadables)
    {
        $this->items = $downloadables;
    }

    public function merge(DownloadableCollection $collection)
    {
        $this->items = array_merge($this->items, $collection->toArray());

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->items);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($key)
    {
        return $this->items[$key];
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($key)
    {
        unset($this->items[$key]);
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function offsetSet($key, $value)
    {
        throw_if(!$value instanceof Downloadable, new TypeError('Value must be of type Downloadble'));

        if (is_null($key)) {
            $this->items[] = $value;
        } else {
            $this->items[$key] = $value;
        }
    }

    /**
     * @inheritDoc
     */public function toArray()
    {
        return $this->items;
    }

    /**
     * @inheritDoc
     */public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    public function count()
    {
       return count($this->items);
    }
}
