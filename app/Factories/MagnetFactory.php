<?php


namespace App\Factories;


use App\SearchProviders\Downloadable;
use Exception;


class MagnetFactory
{
    static $trackers
        = [
            'http://tracker.tntvillage.scambioetico.org:2710/announce',
            'udp://tracker.tntvillage.scambioetico.org:2710/announce',
            'udp://tracker.coppersurfer.tk:6969/announce',
            'udp://tracker.leechers-paradise.org:6969/announce',
            'udp://IPv6.leechers-paradise.org:6969/announce',
            'udp://tracker.internetwarriors.net:1337/announce',
            'udp://tracker.tiny-vps.com:6969/announce',
            'udp://tracker.mg64.net:2710/announce',
            'udp://tracker.openbittorrent.com:80/announce',
            'udp://tracker.internetwarriors.net:1337/announce',
            'udp://tracker.leechers-paradise.org:6969/announce',
            'udp://tracker.coppersurfer.tk:6969/announce',
            'udp://tracker.pirateparty.gr:6969/announce',
            'http://explodie.org:6969/announce',
            'http://torrent.nwps.ws/announce',
            'udp://tracker.cyberia.is:6969/announce',
        ];

    public static function make(string $hash, string $title, string $file_size): string
    {
        return "magnet:?xt=urn:btih:$hash&tr=" . implode(
                '&tr=', static::$trackers
            ) . "&dn=$title&xl=$file_size";
    }
}
