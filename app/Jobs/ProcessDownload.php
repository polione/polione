<?php


namespace App\Jobs;


use App\Contracts\SearchProviders\Downloadable;
use App\Models\Stat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessDownload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * @var Downloadable
     */
    protected $downloadable;

    public function __construct(Downloadable $downloadable)
    {
        $this->downloadable = $downloadable;
    }

    public function handle()
    {

        if(!Stat::isNew($this->downloadable)){
           $this->downloadable->handle();
        }

        Stat::create(
            [
                "name" => $this->downloadable->name ?? "NO NAME",
                "season" => $this->downloadable->season ?? 0,
                "episode" => $this->downloadable->episode ?? 0,
                "timestamp" => now(),
                "path" => realpath($this->downloadable->getDirectory()),
                "filename" => $this->downloadable->getFilename()
            ]
        );
    }
}
