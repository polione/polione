<?php

namespace App;

use App\Collections\DownloadableCollection;
use App\Contracts\SearchProviders\SearchProvider;
use Illuminate\Console\OutputStyle;
use Illuminate\Support\Facades\Log;

class Polione
{

    /**
     * @var array<SearchProvider>
     */
    private $providers = [];
    private OutputStyle $output;

    public function __construct()
    {
        $this->output = new NullOutput();
    }

    public function setOutput(OutputStyle $output)
    {
        $this->output = $output;
    }

    public function search(string $query = null): array
    {

        $bar = $this->output->createProgressBar(count($this->providers));
        $results = new DownloadableCollection();
        $bar->setFormat('verbose');
        $bar->start();

        foreach ($this->providers as $provider) {
            try {
                $results->merge($provider->search($query));
            }catch (\Exception $e){
                Log::error(get_class($provider) . ': '. $e->getMessage());
            }
            $bar->advance();
        }
        $bar->finish();
        $this->output->newLine(2);
        return $results->toArray();
    }

    public function addProvider(SearchProvider $provider) : self
    {
        $this->providers[] = $provider;

        return $this;
    }

    public function addProviders(array $providers) : self
    {
        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }

        return  $this;
    }
}
