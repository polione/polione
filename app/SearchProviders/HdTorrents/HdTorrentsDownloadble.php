<?php

namespace App\SearchProviders\HdTorrents;

use App\SearchProviders\Downloadable;
use Exception;

class HdTorrentsDownloadble extends Downloadable
{
    private $title;
    private $magnet;
    private $category;

    public $name;
    public $season;
    public $episode;

    public function __construct(
        string $title, string $magnet, string $category = 'TV Shows'
    )
    {
        $this->createdBy = "HDTorrents";
        $this->title = $title;
        $this->magnet = $magnet;
        $matches = str_regex($title, '/([ .+a-zA-Z0-9&]*?)[ .+]*(?:S|s)?(\d{1,2})(?:(?:E|e|x|X)(\d{1,2}(?:-\d{1,2})?))*/');
        $this->category = $category;
        try {
            // we skip the first position of the array
            [, $this->name, $this->season, $this->episode] = array_map(
                'str_clear', $matches
            );
            $this->name = trim(str_replace("S$this->season", '', $this->name));


        } catch (Exception $e) {

        }
    }

    public function getFilename(): string
    {
        return $this->name . (empty($this->season) ? '' : " S$this->season")
            . " E" . $this->episode;
    }

    public function getDirectory(): string
    {
        return "/media/disk5" .
            "/" . $this->category .
            "/" . $this->name .
            ((empty($this->season)
                ? ''
                : "/" . "Season " . ltrim(
                    $this->season, '0'
                ))) .
            "/";
    }

    public function handle()
    {
        $tc = app('transmission');
        $context = stream_context_create(
            [
                'http' => [
                    'header' => "Cookie: uid=57205; pass=7bc8952b161e3ae6030ce584aa37e610",
                ]
            ]
        );
        $torrent = file_get_contents($this->magnet, false, $context);
        $tc->addFile($torrent, $this->getDirectory());
    }

    public function getTitle(): string
    {
        return $this->title;
    }


}
