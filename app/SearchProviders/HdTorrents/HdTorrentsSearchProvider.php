<?php


namespace App\SearchProviders\HdTorrents;


use App\Collections\DownloadableCollection;
use App\SearchProviders\SearchProvider;
use DOMDocument;
use DOMElement;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class HdTorrentsSearchProvider extends SearchProvider
{
    /**
     * @var string
     */
    private $uri;

    public function __construct(
        string $uri = 'https://hdtorrents.xyz/rss.php?feed=dl&cat=3,1&passkey=56f3bf0edcab88362745c4b684030f67'
    ) {
        $this->uri = $uri;
    }

    public function search(string $query = null): DownloadableCollection
    {
        $results = new DownloadableCollection();

        try {
            $document = new DOMDocument();
            $ctx = stream_context_create(['http'=>
                [
                    'timeout' => 3,  // Seconds
                ],
                "ssl"=>[
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ]
            ]);

            $document->loadXML(
                str_replace('&name', '&amp;name',
                    str_replace('<?php','<?',
                        file_get_contents($this->uri,false,$ctx)
                    )

                )
            );


            /** @var DOMElement $item ; */
            foreach ($document->getElementsByTagName('item') as $item) {
                $title = $item->getElementsByTagName('title')[0]->textContent;
                $magnet = $item->getElementsByTagName('link')[0]->textContent;

                if (Str::contains(strtolower($title), strtolower($query)) > 0) {
                    $results[] = new HdTorrentsDownloadble($title, $magnet);
                }
            }
        } catch (Exception $e) {

            Log::warning('Errore' . $e->getMessage());
        }
        return $results;
    }
}
