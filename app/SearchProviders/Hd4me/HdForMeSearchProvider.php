<?php

namespace App\SearchProviders\Hd4me;

use App\Collections\DownloadableCollection;
use App\SearchProviders\SearchProvider;
use DOMDocument;
use Illuminate\Support\Str;

class HdForMeSearchProvider extends SearchProvider
{


    private string $cache_file ="/hdforme.cache";
    private string $url = "https://hd4me.net/lista-statica";

    /**
     * @var domDocument
     */
    private domDocument $dom;

    public function __construct()
    {
        $this->cache_file  = sys_get_temp_dir() . $this->cache_file;

    }

    public function search(string $query = null): DownloadableCollection
    {

        $result = new DownloadableCollection();
        $this->dom = new domDocument;
        /*** load the html into the object ***/
        @$this->dom->loadHTML($this->cache());
        /*** discard white space ***/
        $this->dom->preserveWhiteSpace = false;
        /*** the table by its tag name ***/
        $tables = $this->dom->getElementsByTagName('ul');
        /*** get all rows from the table ***/
        $rows = $tables->item(5)->getElementsByTagName('li');
        /*** loop over the table rows ***/
        foreach ($rows as $row) {
            //$x = $row->getElementsByTagName("/br")[0];
            $y = $row->getElementsByTagName("h3")[0];
            if ($y) {
                $row->removeChild($y);
            }
           /* if ($x) {
                $row->removeChild($x);
            }*/
            /*** get each column by tag name ***/
            $query = Str::lower($query);
            $title = Str::lower(trim($row->nodeValue));

            if (Str::contains($title,$query)) {
                $link = $row->getElementsByTagName('a')->item(0)
                    ->getAttribute('href');
                $result[] = new HdForMeDownloadable($title, $link);
            }
        }
        return $result;

    }


    private function cache()
    {
        if (file_exists( $this->cache_file)
            && (filemtime($this->cache_file) > (time() - 60 * 60))
        ) {
            // Cache file is less than five minutes old.
            // Don't bother refreshing, just use the file as-is.
            $file = file_get_contents($this->cache_file);
        } else {
            // Our cache is out-of-date, so load the data from our remote server,
            // and also save it over our cache for next time.
            $file = file_get_contents($this->url);
            file_put_contents($this->cache_file, $file, LOCK_EX);
        }
        return $file;
    }
}