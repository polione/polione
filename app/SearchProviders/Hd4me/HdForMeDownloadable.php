<?php

namespace App\SearchProviders\Hd4me;

use App\SearchProviders\Downloadable;
use DOMDocument;

class HdForMeDownloadable extends Downloadable

{
    private $title;
    public $link;
    public $size;
    public $internalCategory = "Movie";
    public function __construct($title, $link)
    {

        $this->link = $link;
        $this->createdBy = ucwords(parse_url($this->link)['host']);
        preg_match(
            '/(\d*\.?\d+)\s*(?(?=[KMGT])([KMGT])(?:i?B)?|B?)$/i', $title,
            $matches
        );

        $title = str_replace($matches[0], '', $title);
        $this->size = strtoupper($matches[0]);
        $last_bracket = strpos($title, ')') + 1;
        $this->detail = trim(
            substr(
                $title, $last_bracket, strlen($title) - $last_bracket
            )
        );
        $this->title = str_replace($this->detail, '', $title);
    }


    public function getTitle(): string
    {
        return $this->title;
    }

    public function getFileName(): string
    {
        return $this->title;
    }

    public function getDirectory(): string
    {
        if ($this->internalCategory) {
            return "/media/disk4/Movies/";
        }
        return '';
    }


    public function handle()
    {
        echo $this->getPage() . PHP_EOL;
    }

    public function getPage()
    {
        $page = file_get_contents($this->link);
        $dom = new domDocument;
        @$dom->loadHTML($page);
        $download_row = $dom->getElementById('marg');
        $mega = $download_row->getElementsByTagName(
            'li'
        )[1]->getElementsByTagName(
            'a'
        )[0]->getAttribute('href');
        return trim(
            str_replace('https://hd4me.net/?', 'https://mega.nz/#', $mega)
        );
    }

}

