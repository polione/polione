<?php


namespace App\SearchProviders\Horrible;


use App\SearchProviders\Downloadable;

class HorribleDownloadable extends Downloadable
{
    private $title;
    private $magnet;
    private $category;

    public $name;
    public $season;
    public $episode;

    public function __construct(string $title, string $magnet, string $category = 'TV Shows')
    {
        $this->createdBy = "Horrible";
        $this->title = $title;
        $this->magnet = $magnet;
        $matches = str_regex($title, '/\] (.*(?:S(\d))|.*) - (\d+)/');
        $this->category = $category;

        try {
            // we skip the first position of the array
            [, $this->name, $this->season, $this->episode] = array_map('str_clear', $matches);
            $this->name = trim(str_replace("S$this->season", '', $this->name));
        } catch (\Exception $e) {
        }
    }

    public function getFileName(): string
    {
        return $this->name . (empty($this->season) ? '' : " S$this->season") . " E" . $this->episode;
    }

    public function getDirectory(): string
    {
        return "/media/disk5" .
            "/" . $this->category .
            "/" . $this->name .
            ((empty($this->season) ? '' : "/" . "Season " . ltrim($this->season, '0'))) .
            "/";
    }

    public function handle()
    {
        $tc = app('transmission');
        $tc->addUrl($this->magnet, $this->getDirectory());
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
