<?php


namespace App\SearchProviders\IlCorsaroNero;


use App\Factories\MagnetFactory;
use App\SearchProviders\Downloadable;
use Illuminate\Support\Str;

class IlCorsaroNeroDownloadable extends Downloadable
{

    public string $name;
    public string $season;
    public string $episode;
    private string $category;

    public string $internalCategory;
    public string $size;
    public string $date;
    public string $hash;
    public string $seeders;
    public string $leeches;
    private string $title;


    public function __construct($title)
    {
        $this->createdBy = "IlCorsaroNero";
        $this->category = "TV Shows";

        $this->title = trim($title);

        try {
            $matches = str_regex($this->title,'/([ .+a-zA-Z0-9&]*?)[ .+]*(?:S|s)?(\d{1,2})(?:(?:E|e|x|X)(\d{1,2}(?:-\d{1,2})?))*/');
            // we skip the first position of the array
            [, $this->name, $this->season, $this->episode] = array_map('str_clear', $matches);
        } catch (\Exception $e) {
            $this->name = $this->title;
            $this->season = 0;
            $this->episode = 0;
        }
    }

    public function getTitle(): string
    {
        return trim($this->title);
    }

    public function getFileName(): string
    {
        return trim($this->name);
    }

    public function handle()
    {
        $tc = app('transmission');
        $tc->addUrl($this->getMagnet(), $this->getDirectory());
    }

    public function getMagnet()
    {
        return MagnetFactory::make(
            $this->hash, $this->getFileName(), 0
        );
    }
    public function getInternalCategory(){
        $internalCategory = str_clear($this->internalCategory);
        $internalCategory = Str::lower($internalCategory);
        return $internalCategory;
    }
    public function getDirectory(): string
    {
       

        if(Str::contains($this->getInternalCategory(), "ebooks")){
          
            return "/media/disk3/Ebook/";
        }
        if(!Str::contains($this->getInternalCategory(), "serietv")){
            return "/media/disk4/Movies/";
        }
        return "/media/disk5" .
            "/" . $this->category .
            "/" . $this->getFileName() .
            "/" . "Season " . ltrim($this->season, '0') .
            "/";
    }





}
