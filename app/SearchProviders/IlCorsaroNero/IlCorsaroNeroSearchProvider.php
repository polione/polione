<?php


namespace App\SearchProviders\IlCorsaroNero;


use App\Collections\DownloadableCollection;
use App\SearchProviders\SearchProvider;
use DOMXPath;

class IlCorsaroNeroSearchProvider extends SearchProvider
{
    /**
     * @var string
     */
    private string $url;

    public function __construct($url = 'https://ilcorsaronero.pw/argh.php')
    {
        $this->url = $url;
    }

    public function search(string $query = null): DownloadableCollection
    {
        $result = new DownloadableCollection();
        libxml_use_internal_errors(true);

        if (!is_file($this->url)) {
            $this->url .= "?search=" . urlencode($query);
        }

        $document = new \DOMDocument();
        $document->loadHTMLFile($this->url);

        $xpath = new DOMXPath($document);
        $elements = $xpath->query("//tr[contains(@class, 'odd')]");

        foreach ($elements as $element) {
            try {
                $tds = $element->childNodes;

                // We pass the title of the item, we need it for parsing
                // other info with the regex. Also we fallback to it
                // if we can't find the real name of the thing.
                $download = new IlCorsaroNeroDownloadable($tds[3]->nodeValue);

                $download->internalCategory = $tds[1]->nodeValue;
                $download->size = $tds[4]->nodeValue;
                $download->date = $tds[8]->nodeValue;
                $download->seeders = $tds[10]->nodeValue;
                $download->leeches = $tds[12]->nodeValue;
                $download->hash = $xpath->query('.//input[contains(@class,\'downarrow\')]/@value', $element)
                                        ->item(0)->nodeValue;
                $result[] = $download;

            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        return $result;
    }
}
