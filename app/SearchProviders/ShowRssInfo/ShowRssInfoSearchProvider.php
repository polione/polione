<?php


namespace App\SearchProviders\ShowRssInfo;


use App\SearchProviders\SearchProvider;
use App\Collections\DownloadableCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ShowRssInfoSearchProvider extends SearchProvider
{
    /**
     * @var string
     */
    private $uri;

    public function __construct(string $uri = 'http://showrss.info/user/93120.rss?magnets=true&namespaces=true&name=null&quality=null&re=null')
    {
       $this->uri = $uri;
    }

    public function search(string $query = null): DownloadableCollection
    {

        $results = new DownloadableCollection();
        try {
            $document = new \DOMDocument();
            $document->load($this->uri);

            /** @var \DOMElement $item ; */
            foreach ($document->getElementsByTagName('item') as $item) {

                $title = $item->getElementsByTagName('raw_title')[0]->textContent;
                $magnet = $item->getElementsByTagName('link')[0]->textContent;


                if (Str::contains(strtolower($title), strtolower($query)) > 0) {
                    $results[] = new ShowRssDownloadable($title, $magnet);
                }
            }
        }catch (\Exception $e) {
            Log::warning('Errore' . $e->getMessage());
        }
        return $results;
    }
}
