<?php


namespace App\SearchProviders\ShowRssInfo;


use App\DirectoryManager\DirectoryManager;
use App\SearchProviders\Downloadable;

class ShowRssDownloadable extends Downloadable
{
    private $title;
    private $magnet;
    public $category;

    public $name;
    public $season;
    public $episode;

    public function __construct(string $title, string $magnet, string $category = 'TV Shows')
    {
        $this->createdBy = "ShowRssInfo";
        $this->title = $title;
        $this->magnet = $magnet;
        $matches = str_regex($title, '/([ .+a-zA-Z0-9]*)[ .+]*S(\d{1,2})E(\d{1,2})/');
        $this->category = $category;
        try {
            // we skip the first position of the array
            [, $this->name, $this->season, $this->episode] = array_map('str_clear', $matches);
        } catch (\Exception $e) {
        }
    }

    public function getFilename(): string
    {
        return $this->name . " S" . $this->season . "E" . $this->episode;
    }

    public function handle()
    {
        $tc = app('transmission');
        $tc->addUrl($this->magnet, $this->getDirectory());
    }

    public function getTitle(): string
    {
        return $this->title . ' - (' . $this->getFilename() . ')';
    }
}
