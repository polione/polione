<?php


namespace App\SearchProviders\Nyaa;


use App\SearchProviders\SearchProvider;
use App\Collections\DownloadableCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class NyaaSearchProvider extends SearchProvider
{
    /**
     * @var string
     */
    private string $uri;

    public function __construct(string $uri = 'https://nyaa.iss.one/?page=rss&c=1_0')
    {
       $this->uri = $uri;
    }

    public function urlQuery($query): string
    {
        $q = str_replace(' ', '+', $query);

        return $this->uri . "&q=$q";
    }

    public function search(string $query = null): DownloadableCollection
    {
        $results = new DownloadableCollection();

        try {
            $document = new \DOMDocument();
            $document->load($this->urlQuery($query));

            /** @var \DOMElement $item ; */
            foreach ($document->getElementsByTagName('item') as $item) {

                $title = $item->getElementsByTagName('title')[0]->textContent;
		$link = $item->getElementsByTagName('link')[0]->textContent;
                $hash = $item->getElementsByTagName('infoHash')[0]->textContent;
                $size = $item->getElementsByTagName('size')[0]->textContent;

                if (Str::contains(strtolower($title), strtolower($query)) > 0) {
                    $nya = new NyaaDownloadable($title);
		    $nya->setLink($link);
                    $nya->setHash($hash);
                    $nya->setSize($size);
                    $results[] = $nya;
                }
            }
        }catch (\Exception $e) {
            Log::warning('Errore' . $e->getMessage());
        }
        return $results;
    }
}
