<?php


namespace App\SearchProviders\Nyaa;


use App\Factories\MagnetFactory;
use App\SearchProviders\Downloadable;

class NyaaDownloadable extends Downloadable
{
    protected string $title;

    public string $category = 'Anime';

    public string $createdBy = 'Nyaa';

    public $name;

    public $season;

    public $episode;


    public function __construct(string $title)
    {
        $this->title = $title;

        $matches = str_regex($title, '/\] (.*(?:S(\d))|.*) - (\d+)/');

        try {
            // we skip the first position of the array
            [, $this->name, $this->season, $this->episode] = array_map('str_clear', $matches);
        } catch (\Exception $e) {
            $this->name = str_clear($this->title);
        }
    }

    public function getFilename(): string
    {
        return $this->name . " S" . $this->season . "E" . $this->episode;
    }

    public function handle()
    {
        $tc = app('transmission');
        $tc->addUrl($this->magnet, $this->getDirectory());
    }

    public function getTitle(): string
    {
        return $this->getFilename();
    }

    public function getMagnetAttribute()
    {
        // return MagnetFactory::make($this->hash, $this->getTitle(), $this->size);
	return $this->link;
    }
}
