<?php

namespace App\SearchProviders;

use App\Contracts\SearchProviders\Downloadable as DownloadableContract;
use App\DirectoryManager\DirectoryCategory;
use App\Facades\DirectoryManager;
use App\Jobs\ProcessDownload;
use Illuminate\Support\Str;
use UnexpectedValueException;

abstract class Downloadable implements DownloadableContract
{
    protected string $createdBy;

    public function download()
    {
        ProcessDownload::dispatch($this);
    }

    public function createdBy(): string
    {
        return $this->createdBy ?? "N/A";
    }

    public function __get($attributeName)
    {
        $attributeName = "get" . Str::camel($attributeName) . "Attribute";

        if (method_exists($this, $attributeName)) {
            return $this->$attributeName();
        }
        return "N/A";
    }

    public function __call($name, $args)
    {
        if (Str::startsWith($name, 'set')) {
            $attribute = strtolower(str_replace('set', '', $name));
            $this->$attribute = $args[0];
            return;
        }

        throw new \RuntimeException('No defined method');
    }

    public function getField(string $field) :string
    {
        return $this->$field;
    }

    public function getPathAttribute(): string {
        return $this->getDirectory();
    }

    public function getCategory(): DirectoryCategory
    {
        try {
            return new DirectoryCategory($this->category);
        } catch (UnexpectedValueException $e) {
            return DirectoryCategory::MISC();
        }
    }

    public function getDirectory(): string
    {
        return DirectoryManager::with($this);
    }
}
