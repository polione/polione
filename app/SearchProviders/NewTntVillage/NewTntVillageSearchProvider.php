<?php

namespace App\SearchProviders\NewTntVillage;

use App\Collections\DownloadableCollection;
use App\SearchProviders\SearchProvider;
use DOMDocument;

class NewTntVillageSearchProvider extends SearchProvider
{
    /**
     * @var string
     */
    private string $url;

    public function __construct(
        $url = 'http://tntvillage.scambioetico.org/src/releaselist.php'
    )
    {
        $this->url = $url;
    }

    /**
     * @param string|null $query
     *
     * @return DownloadableCollection
     */
    public function search(string $query = null): DownloadableCollection
    {
        $result = new DownloadableCollection();
        $postdata = http_build_query(
            array(
                'cat' => '0',
                'page' => '1',
                'srcrel' => $query
            )
        );
        $ctx = stream_context_create(
            [
                'http' =>
                    [
                        "method" => "POST",
                        'content' => $postdata,
                        'timeout' => 10,// Seconds
                        'header' => "Content-type: application/x-www-form-urlencoded; charset=UTF-8\r\n"
                            .
                            "X-Requested-With: XMLHttpRequest\r\n" .
                            "Origin : http://tntvillage.scambioetico.org\r\n" .
                            "Referer: http://tntvillage.scambioetico.org/?releaselist\r\n"
                    ],
                "ssl" => [
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ]
            ]
        );
        $data = file_get_contents($this->url, true, $ctx);
        $data = utf8_decode($data);
        $dom = new domDocument;
        @$dom->loadHTML($data);
        $dom->preserveWhiteSpace = false;
        $tables = $dom->getElementsByTagName('table');
        $rows = $tables->item(0)->getElementsByTagName('tr');
        foreach ($rows as $key => $row) {
            if ($key > 0) {
                $cols = $row->getElementsByTagName('td');
                $downloadable = new NewTntVillageDownloadable(
                    $cols[6]->nodeValue
                );
                $downloadable->magnet = $cols[1]->getElementsByTagName('a')
                    ->item(0)->getAttribute(
                        'href'
                    );
                $categroyImage
                    = $cols[2]->getElementsByTagName('img')->item(0)
                    ->getAttribute(
                        'src'
                    );
                preg_match('/(?<=icon)\d+/', $categroyImage, $matches);
                $downloadable->internalCategory = $matches[0];
                $result[] = $downloadable;

            }
        }
        return $result;
    }
}
