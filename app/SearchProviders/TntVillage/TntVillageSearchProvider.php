<?php


namespace App\SearchProviders\TntVillage;


use App\Collections\DownloadableCollection;
use App\SearchProviders\SearchProvider;
use Illuminate\Support\Facades\DB;

class TntVillageSearchProvider extends SearchProvider
{
    private function cleanQuery($search_term)
    {
        $search_term = "%" . $search_term . "%";

        return str_replace(' ', '%', $search_term);
    }

    public function search(string $query = null): DownloadableCollection
    {
        $results = new DownloadableCollection();

        $query = $this->cleanQuery($query);

        $data = DB::connection('tntvillage')->select(
            DB::connection('tntvillage')->raw(
                "SELECT * FROM (SELECT titolo || ' ' || descrizione AS FULL, * from dump) WHERE FULL LIKE '"
                . $this->cleanQuery($query) . "'"
            )
        );
        foreach ($data as $row) {
            $download = new TntVillageDownloadable($row->FULL);
            $download->data = $row->DATA;
            $download->titolo = $row->TITOLO;
            $download->hash = $row->HASH;
            $download->topic = $row->TOPIC;
            $download->post = $row->POST;
            $download->autore = $row->AUTORE;
            $download->descrizione = $row->DESCRIZIONE;
            $download->dimensione = $row->DIMENSIONE;
            $download->internalCategory =$row->CATEGORIA;
            $results[] = $download;
        }

        return $results;
    }
}
