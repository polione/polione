<?php

namespace App\SearchProviders\TntVillage;

use App\SearchProviders\Downloadable;
use App\Factories\MagnetFactory;
use Exception;

class TntVillageDownloadable extends Downloadable
{
    public string $name;
    public string $season;
    public string $episode;
    private string $category;

    public string $titolo;
    public string $full;
    public string $data;
    public string $hash;
    public string $topic;
    public string $post;
    public string $autore;
    public string $descrizione;
    public string $dimensione;
    public string $internalCategory;
    //could by useful
    private array $InternalAvailablecatagories = [
        'all_categories'=> 0,
        'movies'=> 4,
        'music'=> 2,
        'tv_movies_and_programs'=> 1,
        'documentaries'=> 14,
        'students_releases'=> 13,
        'e_books'=> 3,
        'linux'=> 6,
        'anime'=> 7,
        'cartoons'=> 8,
        'macintosh'=> 9,
        'windows_software'=> 10,
        'pc_game'=> 11,
        'playstation'=> 12,
        'music_video'=> 21,
        'sport'=> 22,
        'theater'=> 23,
        'wrestling'=> 24,
        'various'=> 25,
        'xbox'=> 26,
        'wallpaper_images'=> 27,
        'other_games'=> 28,
        'tv_series'=> 29,
        'comics'=> 30,
        'trash'=> 31,
        'nintendo'=> 32,
        'a_book'=> 34,
        'podcast'=> 35,
        'newsstand'=> 36,
        'mobile'=> 37
    ];
  
    /**
     * TntVillageDownloadable constructor.
     *
     * @param $full
     */
    public function __construct(string $full)
    {
        $this->createdBy = "TntVillage";
        $this->category = "TV Shows";
        $this->full = $full;


        try {
            $matches = str_regex($this->full,'/([ .+a-zA-Z0-9&]*)[ .+]*S(\d{1,2})(?:E|e)(\d{1,2}(?:-\d{1,2})?)/');
            // we skip the first position of the array
            [, $this->name, $this->season, $this->episode] = array_map('str_clear', $matches);
        } catch (Exception $e) {
            $this->name = $this->full;
            $this->season = 0;
            $this->episode = 0;
        }
    }

    public function getTitle(): string
    {
        return $this->titolo;
    }

    public function getFileName(): string
    {
        return $this->titolo;
    }
    public function getDetailAttribute(): string
    {
        return $this->descrizione;
    }
    public function handle()
    {
        $tc = app('transmission');
        $tc->addUrl($this->getMagnet(), $this->getDirectory());
    }

    public function getMagnet()
    {
        return MagnetFactory::make($this->hash, $this->getFileName(), $this->dimensione);
    }

    public function getDirectory(): string
    {
        if($this->internalCategory == 4) {
            return "/media/disk4/Movies/";
        }
        if($this->internalCategory == 3) {
            return "/media/disk3/Ebook/";
        }
        return "/media/disk5" .
            "/" . $this->category .
            "/" . $this->name .
            "/" . "Season " . ltrim($this->season, '0') .
            "/";
    }

    public function getSizeAttribute(){
        return bytesToHuman($this->dimensione);
    }

}
