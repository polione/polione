<?php

namespace App\Commands;
class DownloadableMake extends AbstractCommandProviderGenerator
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:downloadable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new downloadable';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Downloadable';


    public function handle()
    {
        if (parent::handle() === false && !$this->option('force')) {
            return false;
        }

    }
}
