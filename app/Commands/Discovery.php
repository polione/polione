<?php

namespace App\Commands;

use App\Models\Provider;
use App\Support\CC;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Discovery extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'discovery';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Discovery New Search Providers ';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $available = [];
        $search = [];

        foreach (config('app.search') ?? [] as $provider) {
            $dbProvider = Provider::query()->where('type', '=', "\\$provider");
            if ($dbProvider->exists()) {
                $search[] = [$provider, CC::make('Configured')->green(),$dbProvider->first()->enabled];
            } else {
                $available[] = $provider;
                $search[] = [$provider, CC::make('Can Be Added')->yellow(),false];

            }
        };

        $this->table(['Available Providers', 'Is New','Enabled'], $search);

        if ($this->confirm("Configure all providers?", "yes")) {
            collect($available)->each(function ($provider) {
                Provider::create([
                    'type' => "\\$provider",
                    'link' => ''
                ]);
            });
            $this->info('Done!');
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
