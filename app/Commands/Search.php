<?php

namespace App\Commands;

use App\Facades\PolioneSearch;
use App\Models\Provider;
use App\Support\Loop;
use App\Support\Range;
use App\Support\Table;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Search extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'search {query=""} {--with=""}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'It can search anything, just ask';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() : void
    {


        $query = $this->argument('query');

        $providers = Provider::all()->map->make()->filter()->toArray();

        $search = PolioneSearch::addProviders($providers);
        $search->setOutput($this->output);
        if (empty($query)) {
            return;
        }
        $table = new Table($this->output, $this->options());

        Loop::with($query)->do(function ($query) use ($search, $table) {
            $results = $search->search($query);


            if (count($results) <= 0) {
                return 'exit';
            }
            $table->render($results);
            $range = $this->ask('What is your choice?');

            if (is_null($range)) {
                // the range is null so we return the same query
                // it is a bit costly because we will fetch
                // the same things but for now it's ok.
                return $query;
            }

            $selected = Range::get($range, $results);

            if (count($selected) > 0) {
                foreach ($selected as $item) {
                    $item->download();
                    $this->output->writeln($item->getFileName() . ' dispatched!');
                }
                return 'exit';
            }

            return $range;
        });

    }

    /**
     * Define the command's schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
