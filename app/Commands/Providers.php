<?php

namespace App\Commands;

use App\Models\Provider;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Providers extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'providers';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'List all providers';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->table([], Provider::all(['type', 'link','enabled'])->toArray());
    }

    /**
     * Define the command's schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
