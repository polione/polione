<?php

namespace App\Commands;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

abstract  class AbstractCommandProviderGenerator extends  GeneratorCommand
{
    protected function getStub()
    {
        return __DIR__ . '/stubs/' . $this->type . '.stub';

    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\SearchProviders\\' . $this->getNameInput();
    }
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        return $this->laravel['path'] . '/' . str_replace('\\', '/', $name)
            . $this->type . '.php';
    }

    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace(['DummyClass', '{{ class }}', '{{class}}'], $class . $this->type, $stub);
    }
}