<?php

namespace App\Commands;

use Illuminate\Support\Str;

class ProviderMake extends AbstractCommandProviderGenerator
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $name = 'make:provider';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'SearchProvider';

    public function handle()
    {
        $name = $this->getNameInput();
        if (parent::handle() === false && !$this->option('force')) {
            return;
        }
        $this->call(
            'make:downloadable', [
                'name' => "{$name}",
            ]
        );
    }




}
