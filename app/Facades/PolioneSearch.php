<?php

namespace App\Facades;
use App\Polione;
use App\SearchProviders\Downloadable;
use App\SearchProviders\SearchProvider;
use Illuminate\Console\OutputStyle;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Downloadable[] search(string $query = null)
 * @method static self addProvider(SearchProvider $provider)
 * @method static self addProviders(SearchProvider[] $provider)
 * @method static self setOutput(OutputStyle $output)
 */
class PolioneSearch extends Facade
{
    protected static function getFacadeAccessor()
    {
       return Polione::class;
    }
}
