<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\DirectoryManager\DirectoryManager as DirectoryManagerClass;

/**
 * @method static self with(Downloadble $downloadble)
 */
class DirectoryManager extends Facade
{
    protected static function getFacadeAccessor()
    {
       return DirectoryManagerClass::class;
    }
}
