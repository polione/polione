<?php

namespace App\DirectoryManager;

use App\SearchProviders\Downloadable;
use MyCLabs\Enum\Enum;

class DirectoryCategory extends Enum
{
    public const MOVIE = 'Movies';

    public const TVSHOW = 'TV Shows';

    public const ANIME = 'Anime';

    public const MISC = 'Misc';

    public function getEnvKey()
    {
        return 'DISK_' . $this->getKey();
    }
}

class DirectoryManager
{
    private const BASE_PATH = '/media';

    public function with(Downloadable $downloadble): string
    {
        $category = $downloadble->getCategory();

        $disk = $this->getDisk($category);

        $path = self::BASE_PATH . $disk . DIRECTORY_SEPARATOR . $category;

        if (in_array($category, [DirectoryCategory::TVSHOW, DirectoryCategory::ANIME])) {

            $path .=  DIRECTORY_SEPARATOR . $downloadble->name .
                DIRECTORY_SEPARATOR . "Season " . ltrim($downloadble->season, '0');
        }

        return $path;
    }

    protected function getDisk($category)
    {
        return DIRECTORY_SEPARATOR . env($category->getEnvKey(), 'disk3');
    }
}
