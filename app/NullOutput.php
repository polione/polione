<?php

namespace App;
use Illuminate\Console\OutputStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NullOutput extends OutputStyle
{
    public function __construct(InputInterface $input = null, OutputInterface $output = null)
    {

    }

    public function createProgressBar(int $count = 0)
    {
        return new NullProgressBar();
    }

    public function newLine(int $int = 1)
    {

    }
}

