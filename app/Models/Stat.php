<?php

namespace App\Models;

use App\Contracts\SearchProviders\Downloadable;
use Codedungeon\PHPCliColors\Color;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $guarded  = [];

    public $timestamps = false;

    public static function isNew(Downloadable $show)
    {
        $count = Stat::where('name', $show->getFileName())
                    ->where('season', $show->season)
                    ->where('episode', $show->episode)
                    ->count();

//        if ($count > 0) {
//            echo Color::YELLOW . '✅  ' . $show->getFileName() . Color::RESET . PHP_EOL;
//        } else {
//            echo Color::YELLOW . '🚀  ' . $show->getFileName() . ' in: ' . $show->getDirectory() . Color::RESET . PHP_EOL;
//        }

        return $count > 0;
    }
}
