<?php

namespace App\Models;

use App\SearchProviders\SearchProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Provider extends Model
{
    protected $guarded = [];

    public function make(): ?SearchProvider
    {
        if (!class_exists($this->type)) {
            Log::warning('Check search provider ' . $this->type . ', it is not instantiable');
            return null;
        }

        if (empty($this->link)) {
            return new $this->type;
        }

        return new $this->type($this->link);
    }

    public function enable(){
        $this->enabled = true;
        $this->save();
    }
    public function disable(){
        $this->enabled = false;
        $this->save();
    }
}
