<?php


namespace App\Contracts\SearchProviders;


use App\Collections\DownloadableCollection;

interface SearchProvider
{
    public function search(string $query = null): DownloadableCollection;
}
