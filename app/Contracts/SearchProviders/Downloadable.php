<?php


namespace App\Contracts\SearchProviders;

use App\DirectoryManager\DirectoryCategory;

interface Downloadable
{
    public function getTitle(): string;

    public function getFileName(): string;

    public function getDirectory(): string;

    public function getCategory(): DirectoryCategory;

    public function createdBy(): string;

    public function getField(string $field): string;

    public function download();

    public function handle();
}
