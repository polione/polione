<?php

if (!function_exists('str_clear')) {
    function str_clear($str)
    {
        $str = str_replace('+', ' ', $str);
        $str = str_replace('.', ' ', $str);
        return trim($str);
    }
}

if (!function_exists('str_regex')) {
    function str_regex($str, $regex): array
    {
        $matches = [];
        preg_match($regex, $str, $matches);
        return $matches;
    }
}

if (!function_exists('str_parse')) {
    function str_parse($str, $key = null)
    {
        $output = [];
        parse_str($str, $output);
        return $key ? $output[$key] : $output;
    }
}

if (!function_exists('str_parse')) {
    function str_parse($str, $key = null)
    {
        $output = [];
        parse_str($str, $output);
        return $key ? $output[$key] : $output;
    }
}

if (!function_exists('bytesToHuman'))
{
    function bytesToHuman($bytes, $decimals = 2)
    {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }
}